import React from 'react';
import {Link} from 'react-router-dom';


const Navbar = () => (
  <div>
  <h3>Ilmoittautuminen</h3>
  <ul>
  <li><Link to={{pathname:'/'}}>Ilmoittautuneet</Link></li>
  <li><Link to={{pathname: '/form'}}>Ilmoittautuminen</Link></li>
  </ul>
  </div>
)

export default Navbar
