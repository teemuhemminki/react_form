import React from 'react';
import Row from '../components/Row'
import {Table, striped, bordered, condensed, hover} from 'react-bootstrap'

// data saadaan parametrina storesta ListContainerin välityksellä
// Lisäarvot asetettu
const List = ({data}) =>  (
<Table striped bordered condensed hover>
  <thead>
    <tr>
      <th>ID</th>
      <th>Nimi</th>
      <th>Email</th>
      <th>Ruokavalinta</th>
      <th>Sauna</th>
    </tr>
  </thead>
  <tbody>
    {data.map (person => (
      <Row key={person.id} {...person} />
    ))}
  </tbody>
</Table>
)

List.propTypes = {
    data: React.PropTypes.array
};

export default List
