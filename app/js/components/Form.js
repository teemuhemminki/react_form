import React from 'react';
import { Field, reduxForm } from 'redux-form'
import {withRouter, Redirect} from 'react-router-dom'
import {FormGroup, FormControl, ControlLabel, DropdownButton, MenuItem, Checkbox} from 'react-bootstrap'

var redirect = false;

/*
Toteutetaan kutsuttavat funktiot kullekin react-bootstrapin form komponentille, jotta nämä toimisivat
react-formin fieldin kanssa yhdessä. Huomaa että kukin palauttaa arvonsa input arvon kautta.
*/
const FormTextInput = ({ type, input }) => {
    return (
      <FormControl
        type = {type}
        onChange={input.onChange}
      />
    )
  }

  //Tämän olisi parempi hakea ja tulostaa arvot taulukosta, kuin että arvot ovat kovakoodattuna.
  const FoodMenu = ({ type, input }) => {
    return (
      <FormControl componentClass = "select" placeholder = "select" onChange={input.onChange}>
        <option value="Kala">Kala</option>
        <option value="Liha">Liha</option>
        <option value="Kasvis">Kasvis</option>
      </FormControl>
    )
  }

  const CheckboxForm = ({ type, input }) => {
    return (
      <Checkbox inline onChange={input.onChange}>Sauna</Checkbox>
    )
  }

//Asetettu uusia arvoja.
let FormBase = props => {
  const { handleSubmit } = props
  if (!redirect) {
    return (
      <form onSubmit={ handleSubmit }>
        <FormGroup>
          <div>
            <label htmlFor="name">  Nimi  </label>
            <div>
            <Field name="name" component={FormTextInput} type="text" />
            </div>
          </div>
          <div>
            <label htmlFor="email"> Email </label>
            <div>
            <Field name="email" component={FormTextInput} type="text" />              
              </div>            
          </div>
          <div>
            <label htmlFor="food"> Ruoka </label>
            <div>
            <Field name="food" component={FoodMenu} type="select" />
              </div>            
          </div>          
          <div>
            <label htmlFor="sauna"> Osallistun saunailtaan </label>
            <div>
            <Field name="sauna" component={CheckboxForm} type="checkbox" />              
              </div>            
          </div>          
          <div>
          <button type="submit"> Lähetä </button>
          </div>
        </FormGroup>  
      </form>
    )
  } else {
    redirect = false;
    return (
      <Redirect push to="/"/>
    )
  }

}

FormBase.propTypes = {
    handleSubmit: React.PropTypes.func.isRequired
};
// FormBase -komponentti yhdistetään reduxForm-kirjastoon
// Tähän olisi hyvä lisätä validoija, joka tarkistaa annettavan datan tyyppiä, ennen lähettämistä.
FormBase = reduxForm({
  form: 'entry',
  onSubmitSuccess: () => {
       console.log('Lähetys onnistui');
       redirect = true;
   }
})(FormBase)

const Form = withRouter(FormBase)

export default Form;
