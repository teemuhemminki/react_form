import React from 'react';

//Lisätty eri arvoille tilat
const Row = (person) =>
(
  <tr>
    <td>{person.id}</td><td>{person.name}</td><td>{person.email}</td><td>{person.food}</td><td>{person.sauna}</td>
  </tr>
);

export default Row;
