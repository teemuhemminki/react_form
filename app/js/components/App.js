import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Navbar from './Navbar'
import ListContainer from '../containers/ListContainer'
import FormContainer from '../containers/FormContainer'
import createBrowserHistory from 'history/createBrowserHistory'

var styles = {margin: '2%'}
// App on tässä reitityskomponentti
const App = () => (

  <Router history={createBrowserHistory()}>
    <div style={styles}>
      <header>
        <Navbar />
      </header>
      <Route exact={true} path='/' render={() => <ListContainer  />} />
      <Route path='/form' render={() => <FormContainer />} />
    </div>
  </Router>

);

export default App;
