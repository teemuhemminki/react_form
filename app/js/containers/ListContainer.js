import React from 'react';
import List from '../components/List'
import {connect} from 'react-redux'

const mapStateToProps = state => {
  return {
    data: state.entries
  }
}

const ListContainer = connect(mapStateToProps)(List)

export default ListContainer
