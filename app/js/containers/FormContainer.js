import React from 'react';
import { connect } from 'react-redux'
import Form from '../components/Form'
import addEntry from '../actions'

//formcontainer saa parametrina dispatch -funktion,
//jolla dispatchataan eli välitetään action addEntry
let FormContainer = ({dispatch}) => {
  return (
  <Form onSubmit={values => {
    dispatch(addEntry(values))
  }} />
)}

FormContainer.propTypes = {
    dispatch: React.PropTypes.func.isRequired
};

// Formcontainer yhdistetty storeen connectilla
FormContainer = connect()(FormContainer);

export default FormContainer
