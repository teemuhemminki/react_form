import {combineReducers} from 'redux'
import { reducer as formReducer } from 'redux-form'

// Reducer ottaa vastaan actionin ja suorittaa actionin vaatiman entries-metodin
const entries = (state = [], action) => {
  switch(action.type) {
    case 'ADD_ENTRY':
    // palautetaan uusi state jossa ovat vanhan staten oliot joiden perään tulee uusi olio
    return [...state, { // ...state purkaa taulukosta oliot
      id: state.length + 1, // id määäräytyy statessa jo olevien olioiden määrän mukaan.
      name: action.data.name,
      email: action.data.email,
      food: action.data.food,
      sauna: action.data.sauna ? "kyllä" : "ei" //Katsotaan onko true vai false ja annetaan arvo sen mukaan
    }];
    // jos ei tullut uutta actionia, palautetaan vanha state
    default:
    return state;
  }

}
// yhdistää reducerit
const entryApp = combineReducers({
  entries: entries,
  form: formReducer// tulee suoraan kirjastosta
})

export default entryApp;
