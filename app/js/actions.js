/*Action on olio joka määrittää
mitä muuttujia storessa voi muuttaa */

const ADD_ENTRY = 'ADD_ENTRY'

//Action olio ei enää talleta id arvoa.

const addEntry = data => {
  return ({type: ADD_ENTRY, data})
}

export default addEntry
