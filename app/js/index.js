import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import entryApp from './reducers'

/*Sovelluksessa on käytetty redux-form -kirjastoa, mikä
tekee siitä hiukan erilaisen verrattuna tavalliseen redux-sovellukseen
Flux-periaate toimii kuitenkin samalla tavalla.
*/

  //Sovellus hakee tallennetun datan localstoragesta, mikäli sitä löytyy
  const persistedState = localStorage.getItem('state') ? JSON.parse(localStorage.getItem('state')) : {}

  let store = createStore(entryApp, persistedState);

  /*Sovellus tallentaa aina muutoksen tapahtuessa localstorageen.
  Tähän voisi laittaa jotain "järkeistys" sääntöjä, jotta tallentelua ei tapahtuisi ihan koko ajan.*/
  store.subscribe(()=>{
    localStorage.setItem('state', JSON.stringify(store.getState()))
  });

// provider sisältää store -olion
ReactDOM.render(
  <Provider store={store}>
    <App>
    </App>
  </Provider>, document.getElementById('app')
);
