# React Form exercise
This project was part of a web application development course in JAMK. The project is based on ready made form, to which I added more fields, localstorage saving and some bootstrap elements.

This project is commented in Finnish.

I don't give any permissions to reuse or copy this code, because most of it is not made by me.